package com.example.proyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLOutput;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView txtAdivinar2;
    private Button btnCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtAdivinar2= (TextView) findViewById(R.id.txtAdivinar);
        btnCheck = (Button) findViewById(R.id.btnCkeck);
        btnCheck.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if(v == btnCheck){
            if(txtAdivinar2.getText().toString().isEmpty()){
                txtAdivinar2.setError("No escrito nada");
            }else{
                //System.out.println("El texto es: " + txtAdivinar2.getText().toString());
                iniciarIntent();
            }

        }
    }

    public void iniciarIntent(){
        try {
            int num = Integer.valueOf(txtAdivinar2.getText().toString());
            Intent intent = new Intent(MainActivity.this,resultado.class);
            intent.putExtra("number", num);
            startActivity(intent);
        }catch (NumberFormatException num){

            txtAdivinar2.setError(getResources().getString(R.string.error_numero));

        }


    }




    }

/*
* public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private TextView txtAdivina;
    private Button btnChek;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtAdivina = (TextView) findViewById(R.id.editTextAdivina);
        btnChek = (Button) findViewById(R.id.btnChek);
        btnChek.setOnClickListener(this);

    }

    @Overrid
    * public void onClick(View v) {
        if(v==btnChek){
            if (txtAdivina.getText().toString().isEmpty()){
                txtAdivina.setError("escribe un número");
            }else{
                verificar();
            }
        }
    }
public int aleatorio(int a){
        Random naletorio= new Random();
        int n= naletorio.nextInt(a);

        return n;

    }
    * public void verificar(){

       try {
           int ntxt= Integer.valueOf(txtAdivina.getText().toString());
           int nal=aleatorio(3);

           if (ntxt== nal){
               Toast.makeText(this,"GANASTE!!!!!", Toast.LENGTH_LONG).show();
           }else{
               Toast.makeText(this,"PERDISTE!!!!! el numero es "+nal, Toast.LENGTH_LONG).show();

           }
       }catch (NumberFormatException n){
           txtAdivina.setError("solo ingresa numeros");

       }*/
