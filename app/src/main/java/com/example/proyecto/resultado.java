package com.example.proyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class resultado extends AppCompatActivity {

    private TextView resultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        resultado = (TextView) findViewById(R.id.txtGanar);


        int numAd = 0;
        Bundle extra = getIntent().getExtras();

        numAd = extra.getInt("number");

        //Toast.makeText(this, "El valor es: " +numAd,Toast.LENGTH_SHORT).show();

        verificar();
    }


        public int aleatorio(int a){
            Random naleatorio = new Random();
            int n = naleatorio.nextInt(a);

            return n;
        }

        public void verificar() {

            int num = 0;
            int nal = aleatorio(3);

            if (num == nal) {
                //System.out.println("Ganaste");
                resultado.setText("!!GANASTES!!");
                resultado.setTextColor(Color.GREEN);
                //Toast.makeText(this, "Ganaste", Toast.LENGTH_LONG).show();
            } else {
                resultado.setText("Perdiste");
                resultado.setTextColor(Color.RED);
                System.out.println("Perdiste");
                Toast.makeText(this, "Perdiste el numero es " + nal, Toast.LENGTH_LONG).show();
            }
        }


        public void IntertarNuevo(View view){
            Intent intent = new Intent(resultado.this, MainActivity.class);
            startActivity(intent);
        }
    }
